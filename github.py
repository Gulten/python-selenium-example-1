from githubUserInfo import username, password
from selenium import webdriver
import time
import os

class Gitgub:
    def __init__(self, username, password):
        os.chmod('/drive/path/chromedriver', 755)
        self.browser = webdriver.Chrome(executable_path='/drive/path/chromedriver')
        self.username = username
        self.password = password
        self.repositories = []
    def signIn(self):
        self.browser.get("https://github.com/login")
        time.sleep(2)
        self.browser.find_element_by_xpath('//*[@id="login_field"]').send_keys(self.username)
        self.browser.find_element_by_xpath('//*[@id="password"]').send_keys(self.password)
        

        time.sleep(1)
    
        self.browser.find_element_by_xpath('//*[@id="login"]/div[4]/form/input[14]').click()

    def getRepositories(self):
        self.browser.get(f'https://github.com/{self.username}?tab=repositories')
        time.sleep(2)
        items = len(self.browser.find_elements_by_css_selector('.col-12.d-flex.public'))
        i=0
        while i < items:
            self.browser.find_elements_by_css_selector('.col-12.d-flex.public')[i].find_element_by_css_selector('.wb-break-all').find_element_by_tag_name('a').click()
            time.sleep(1)
            self.getBranches()
            self.browser.back()
            i += 1

    def getBranches(self):
        self.browser.find_element_by_xpath('//*[@id="js-repo-pjax-container"]/div[2]/div/div[2]/div[1]/div[2]/div[2]/a[1]').click()
        time.sleep(2)
        self.browser.back()

github = Gitgub(username, password)
github.signIn()
github.getRepositories()
github.browser.quit()

